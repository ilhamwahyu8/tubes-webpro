<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Product extends CI_Controller {

	// public function __construct()
 	// {
 	// 	parent::__construct();
 	// 	$this->load->model('M_web');
  	// }

  	// public $data = array(
  	// 	"nim" => "Nim Kalian",
  	// 	"nama" => "Isi Nama Kalian",
  	// 	"kampus"=>"Telkom University"
      // );
      
    public function __construct()
 	{
 		parent::__construct();
 		$this->load->model('M_product');
  	}

	public function index($nama)
	{
		$data_product = $this->M_product->GetOneProduct($nama);
    $this->load->view('V_product',['data'=>$data_product]);
		$this->load->view('V_footer');
    }
    
    public function product($nama)
    {
    	$data_product = $this->M_product->GetOneProduct($nama);
      $this->load->view('V_product',['data'=>$data_product]);
			$this->load->view('V_footer');
    }
}