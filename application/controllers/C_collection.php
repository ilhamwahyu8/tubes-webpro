<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Collection extends CI_Controller {


    public function __construct()
 	{
 		parent::__construct();
 		$this->load->model('M_Collection');
  	}

	public function index()
	{
		$data_product = $this->M_Collection->GetAllProduct();
        $this->load->view('V_Collection',['data'=>$data_product]);
		$this->load->view('V_footer');
    }
    
    public function Collection()
    {
        $data_product = $this->M_Collection->GetAllProduct();
        $this->load->view('V_Collection',['data'=>$data_product]);
		$this->load->view('V_footer');
	}
	
	
}