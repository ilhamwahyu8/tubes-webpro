<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Product extends CI_Model {

	public function GetAllProduct(){
		// $this->db->select('*');
		// $this->db->from('product');
		// $query = $this->db->get();
        // return $query->result();
        $query = $this->db->get('produk');
        return $query->result();
	}

	public function GetOneProduct($nama){
		// $query = $this->db->get('produk');
		// $query = $this->db->where('nama', $nama);
		$nama = str_replace("-"," ","$nama");
		$nama = str_replace("z","&","$nama");
		$query = $this->db->join('fproduk', 'fproduk.nama = produk.nama');
		
		$query = $this->db->get_where('produk', array('produk.nama' => $nama));
		
		return $query->result();
	}
}
