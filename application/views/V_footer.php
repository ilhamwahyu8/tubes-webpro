<!DOCTYPE html>
<html>
<head>
	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- ^ICON -->
    <!-- Bootstrap -->
    <link href="//db.onlinewebfonts.com/c/7bc82f6db541f8f5022ea6eb3b26f78f?family=Mentor+Sans+Std" rel="stylesheet" type="text/css"/>
    
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css') ?>">
    <!-- CSS -->
</head>
<body>

    <footer>
    <div class="container">
        <br>
        <div class="row" >
            <div class="col-sm">
                <li><a class = "footerIsi" href="">Search</a> </li>
                <li><a class = "footerIsi" href="">Shop</a></li>
                <li><a class = "footerIsi" href="">About</a></li>
                <li><a class = "footerIsi" href="">Contact</a></li>
            </div>
            <div class="col-sm">
                <li>
                    <a class = "footerIsi" id="icon" href="https://www.facebook.com/thecardstory"  title="Card Story on Facebook">
                        <span class="fa fa-facebook"></span>
                         &nbspFacebook
                    </a>
                </li>
                <li>
                    <a class = "footerIsi" id="icon" href="https://instagram.com/thecardstory"  title="Card Story on Instagram">
                        <span class="fa fa-instagram"></span>
                        Instagram
                    </a>
                </li>
            </div>
            <div class="col-sm">
                <li style="text-align:right;"><a class = "footerIsi" href="" style="display:inline-block; ">© 2019, Card Story &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</a></li>
            </div>
        </div>
        <br>
    </div>

    </footer>

</body>
</html>