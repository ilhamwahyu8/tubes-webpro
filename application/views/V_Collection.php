<!DOCTYPE html>
<html>
<head>

<title> Collection - Card Story</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/Style.css') ?>">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
    



    
<body>
    <article>
        <h1>lorem</h1>
        <hr>
        <div class="container">        
            <?php
            //Columns must be a factor of 12 (1,2,3,4,6,12)
            $numOfCols = 4;
            $rowCount = 0;
            $bootstrapColWidth = 12 / $numOfCols;
            ?>
            <div class="row">
            <?php
            foreach ($data as $d){
                $link = str_replace(" ","-","$d->nama");
                $link = str_replace("&","z","$link");
            ?>  
                    <div class="col-md-<?php echo $bootstrapColWidth; ?>">
                        <a class="aproduk" href="<?php echo base_url('C_Product/index/'.$link) ?>"  style="color:black"> 
                        <div class="thumbnail">
                            <img class="fproduk" src="assets/img/<?php echo $d->gambar; ?>">
                            <p><?php echo $d->nama; ?> IDR &mdash; <?php echo $d->harga; ?> </p>
                        </div>
                        </a>
                    </div>
            <?php
                $rowCount++;
                if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
            }
            ?>
            </div>
        </div>
        
        
        
        
        
    </article>
        
    
</body>
    
</html>