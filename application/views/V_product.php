<!DOCTYPE html>
<html>
<head>

<?php foreach ($data as $d ) {?>

<title> <?php echo $d->nama ?> - Card Story</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css') ?>">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <!-- ^JQUERY CDN -->
</head>
    



    
<body>
    <article>
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                <img id="change-image" class="gambarproduk" src=" <?php echo base_url('assets/img/'.$d->gambar) ?>">
                <div class="button-container">
                    <button class="0 btng">
                        <img class="gambarlain" src=" <?php echo base_url('assets/img/'.$d->gambar0) ?>">
                    </button>
                    <button class="1 btng">
                        <img class="gambarlain" src=" <?php echo base_url('assets/img/'.$d->gambar1) ?>">
                    </button>
                    <button class="2 btng">
                        <img class="gambarlain" src=" <?php echo base_url('assets/img/'.$d->gambar2) ?>">
                    </button>
                    <button class="3 btng">
                        <img class="gambarlain" src=" <?php echo base_url('assets/img/'.$d->gambar3) ?>">
                    </button>
                    <button class="4 btng">
                        <img class="gambarlain" src=" <?php echo base_url('assets/img/'.$d->gambar4) ?>">   
                    </button>
                </div>
                
                </div>
                <script>
                    jQuery(document).ready(function($){

                $('.0').on({
                    'click': function(){
                        $('#change-image').attr('src','<?php echo base_url('assets/img/'.$d->gambar0) ?>');
                    }
                });
                
                $('.1').on({
                    'click': function(){
                        $('#change-image').attr('src','<?php echo base_url('assets/img/'.$d->gambar1) ?>');
                    }
                });
                
                $('.2').on({
                    'click': function(){
                        $('#change-image').attr('src','<?php echo base_url('assets/img/'.$d->gambar2) ?>');
                    }
                });
                
                $('.3').on({
                    'click': function(){
                        $('#change-image').attr('src','<?php echo base_url('assets/img/'.$d->gambar3) ?>');
                    }
                });
                $('.4').on({
                    'click': function(){
                        $('#change-image').attr('src','<?php echo base_url('assets/img/'.$d->gambar4) ?>');
                    }
                });
                });
                </script>
                <!-- End -->
                <div class="col-lg-5">
                    <h1><?php echo $d->nama ?></h1>
                    <h2> IDR&nbsp <?php echo $d->harga ?> </h2>
                    <hr>
                    <div class="quantity">
                        fungsi quantiity
                    </div>
                    <button class="cart">ADD TO CART</button>
                    <div style="height:10px;"><br></div>                    
                    &nbsp<button class="buy">BUY IT NOW</button>                    
                    <br><br><br><br>
                    <p style="text-align : left;"> <?php echo $d->deskripsi ?> 
                    </p>
                    <br><br><br>
                </div>
                <div class="box"></div>
                
            </div>
            <button class="back">Back to Prev</button>
            <hr>
        </div>
    </article>
        
    
    <?php } ?>
</body>
    
</html>