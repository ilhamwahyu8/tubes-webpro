<!DOCTYPE html>
<html>
<head>
	<title>Home - Card Story</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- ^ICON -->
    <!-- Bootstrap -->
    <link href="//db.onlinewebfonts.com/c/7bc82f6db541f8f5022ea6eb3b26f78f?family=Mentor+Sans+Std" rel="stylesheet" type="text/css"/>
    
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css') ?>">
    <!-- CSS -->
</head>
<body>

<header>    
    <div class="jumbotron" id="jumbotron" style ="background-image : url(<?php echo base_url('assets/img/new.jpg') ?>); ">
    <a class="announcement" href="">
    <p style="padding-top : 14px">All Orders Ship Within 24 Hours | Flat Rate Shipping All Over Indonesia
    </p>
    </a>
  <nav class="navbar navbar-expand-lg navbar-light" style="background-color: transparent ;">
  	<div class="container">
        <div class="col">
            <img src=" <?php echo base_url('assets/img/navputih.PNG') ?>" width="300" height="100" alt="">
        </div>
        <div class="col-6">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" id="navtext" href="#">HOME</a>
                </li>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navtext" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    SHOP</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Art of Play</a>
                    <a class="dropdown-item" href="#">Bicycle®</a>
                    <a class="dropdown-item" href="#">Ellusionist</a>
                    <a class="dropdown-item" href="#">Gemini</a>
                    <a class="dropdown-item" href="#">The Blue Crown</a>
                    <a class="dropdown-item" href="#">Theory11</a>
                    <a class="dropdown-item" href="#">View All</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="navtext" href="#">ABOUT</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="navtext" href='#'>CONTACT</a>
                </li>
            </ul>
        </div>
    </div>
    </nav>
  <br><br><br><br><br><br><br><br><br>
    <p class="jumbotronsub" style="font-size : 28px;">PLAYING CARDS</p>
    <h2 class="jumbotrontitle">Like Never Before</h2>
    <a class="btn btn-primary btn-lg jumbotronbut" href="#" role="button">Shop Now →</a>
  </p>
</div>
</header>

<!-- end nav -->

<!-- Start Main -->
<article>
<div class="container">
        <h1>Featured Collection</h1>
        <hr>
        <h1>All Time Best-Seller</h1>
        <hr>

        <div class="container">
            <div class="row">
                <div class="col">
                <h1 style ="text-align: left;">Our Story</h1>
                <p id = "story">Since 2008 until now, Card Story is the largest premium playing cards in Indonesia and we already served more than 15,000 satisfied customers in Indonesia. We believe that these numbers will continue to grow.</p>
                </div>
                <div class="col">
                <img id= "story" src="<?php echo base_url('assets/img/Our Story.jpg') ?>">
                </div>
            </div>
            <br><br><br><br>
            <div class="row">
                <div class="col">
                <img id= "story" src="<?php echo base_url('assets/img/World Class.jpg') ?>">                </div>
                <div class="col">
                <h1 style ="text-align: left;">World-Class Playing Cards</h1>
                <p id = "story">All of our playing cards stand at the highest quality. Whether you use the cards for magic, cardistry, collections, or send it as a gift, we always guarantee your satisfaction.</p>
                
                </div>
            </div>
        </div>
        <br>

        
        
</div>

<div class="newsletter">
        <div class="container" style="background-color: #EFEEEE">
            <h1>Subscribe to our newsletter</h1>
            <p>Enjoy our newsletter to stay updated with the latest news and special sales.
            <br> Let's put your e-mail address here!</p>
            <hr>
            <br><br>
            <div class="subbox">
                <form action="">
                    <input class="iEmail" type="text" name="emailsubs" value="Your Email" placeholder="Your Email">
                    <button class="sEmail">Subscribe</button>
                </form>
            </div>
            
            <br><br><br><br>
        </div>        
    </div>
    <br><br><br><br>
    <h1>Follow us on Instagram</h1>
    <a href="https://www.instagram.com/thecardstory/" class="ig">@thecardstory</a>
    
    <div class="container">
        <div class="row">
            <div class="col-sm-3 layoutig">
                <a href="https://www.instagram.com/p/BwEvnxOBY2w/" class="toig"> 
                <img class="imgig" src="assets/img/World Class.jpg">
                </a>
                
            </div>
            <div class="col-sm-3 layoutig">
                <a href="https://www.instagram.com/p/BwEvnxOBY2w/" class="toig">
                <img class="imgig" src="assets/img/World Class.jpg">
                </a>
            </div>
            <div class="col-sm-3 layoutig">
                <a href="https://www.instagram.com/p/BwEvnxOBY2w/" class="toig">
                <img class="imgig" src="assets/img/World Class.jpg">
                </a>
            </div>
            <div class="col-sm-3 layoutig">
                <a href="https://www.instagram.com/p/BwEvnxOBY2w/" class="toig">
                <img class="imgig" src="assets/img/World Class.jpg">
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-3 layoutig">
                <a href="https://www.instagram.com/p/BwEvnxOBY2w/" class="toig">
                <img class="imgig" src="assets/img/World Class.jpg">
                </a>
            </div>
            <div class="col-3 layoutig">
                <a href="https://www.instagram.com/p/BwEvnxOBY2w/" class="toig">
                <img class="imgig" src="assets/img/World Class.jpg">
                </a>
            </div>
            <div class="col-3 layoutig">
                <a href="https://www.instagram.com/p/BwEvnxOBY2w/" class="toig">
                <img class="imgig" src="assets/img/World Class.jpg">
                </a>
            </div>
            <div class="col-3 layoutig">
                <a href="https://www.instagram.com/p/BwEvnxOBY2w/" class="toig">
                <img class="imgig" src="assets/img/World Class.jpg">
                </a>
            </div>
        </div>
        <div class="row">
        </div>
        <br><br><br><br>
        <hr>
    </div>

</article>

</body>
</html>