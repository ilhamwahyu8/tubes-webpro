<!DOCTYPE html>
<html>
<head>

    <link rel="shortcut icon" href="<?php echo base_url('assets/img/pageicon.png')?>" type="image/png" />
  
	<title>404 Not Found - Card Story</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- ^ICON -->
    <!-- Bootstrap -->
    
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css') ?>">
    <!-- CSS -->
</head>
<body>
    <header>
    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #ffffff ;">
  	<div class="container">
        <div class="col">
            <img src=" <?php echo base_url('assets/img/Nav.PNG') ?>" width="300" height="100" alt="">
        </div>
        <div class="col-6">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" id="navtext" href="#">HOME</a>
                </li>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navtext" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    SHOP</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Art of Play</a>
                    <a class="dropdown-item" href="#">Bicycle®</a>
                    <a class="dropdown-item" href="#">Ellusionist</a>
                    <a class="dropdown-item" href="#">Gemini</a>
                    <a class="dropdown-item" href="#">The Blue Crown</a>
                    <a class="dropdown-item" href="#">Theory11</a>
                    <a class="dropdown-item" href="#">View All</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="navtext" href="#">ABOUT</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="navtext" href='#'>CONTACT</a>
                </li>
            </ul>
        </div>
    </div>
    </nav>
</header>
<!-- end nav -->

<!-- Start Main -->
<article style="height: 500px;">
    <div class="container">
        <br><br>
        <h1 style="font-family : Mentor Sans;"><strong> 404 Page Not Found</strong></h1>
        <hr>
        <br>
        <p class="keterangan">The page you were looking for does not exist. <a class="click" href="">Click here</a>  to continue shopping.</p>
    </div>    
</article>
<hr>
<br>
<br>


</body>
</html>