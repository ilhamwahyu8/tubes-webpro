<!DOCTYPE html>
<html>
<head>

    <link rel="shortcut icon" href="<?php echo base_url('assets/img/pageicon.png')?>" type="image/png" />
    <title>Contact - Card Story</title>


    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/contact.css') ?>">
    	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- ^ICON -->
    <!-- Bootstrap -->    

</head>

<body>
	<article>
		<div class="col-md-6 offset-md-3">
            <br>
		<h1>Contact</h1>
		<hr> <br>
		<img class="mx-auto d-block" src=" <?php echo base_url('assets/img/Contact.jpg')?> ">
        
        <br>
		<strong><p>CONTACT US</p></strong>
		
		<div>
			<span> 
                <p>
                    Feel free to contact us on our Official Line or Whatsapp : 
                    <br>
                    @thecardstory (use @) / 081808065460
                </p>
                
            </span>	
			
		</div>
            <br>
		<strong><p>MAIL US</p></strong>
		<div> 
			<span> <p>Feel free to mail us on:
			<a class="link" href="mailto:hello@cardstory.co" style="display: block;">
                hello@cardstory.co</a>
                  </p> 
                
            </span>
		</div>
		<strong><p>KEEP IN TOUCH WITH US</p></strong>
		<div> 
			<span> 
                <p>Facebook:  
                    <a class="link" href="http://facebook.com/thecardstory">Card Story</a> <br>
                Instagram: 
                    <a class="link" href="https://www.instagram.com/thecardstory/">@thecardstory</a>  
                </p>  
            </span>
		</div>
            <br><br><br><br>
		<hr>
	</div>
	</article>
	

</body>

</html>